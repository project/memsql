<?php

namespace Drupal\memsql\Driver\Database\memsql;

use Drupal\Core\Database\Driver\mysql\Insert as MysqlInsert;
use Drupal\Core\Database\Query\SelectInterface;

/**
 * MemSQL implementation of \Drupal\Core\Database\Query\Insert.
 */
class Insert extends MysqlInsert {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    if (!$this->preExecute()) {
      return NULL;
    }

    $this->addShardIdInsertValues();

    // If we're selecting from a SelectQuery, finish building the query and
    // pass it back, as any remaining options are irrelevant.
    if (empty($this->fromQuery)) {
      $max_placeholder = 0;
      $values = [];
      foreach ($this->insertValues as $insert_values) {
        foreach ($insert_values as $value) {
          $values[':db_insert_placeholder_' . $max_placeholder++] = $value;
        }
      }
    }
    else {
      $values = $this->fromQuery->getArguments();
    }

    $last_insert_id = $this->connection->query((string) $this, $values, $this->queryOptions);

    // Re-initialize the values array so that we can re-use this query.
    $this->insertValues = [];

    return $last_insert_id;
  }

  /**
   * Adds shard id value if it isn't set for distributed tables.
   */
  protected function addShardIdInsertValues() {
    // No need to add shard key to the reference table.
    // @todo: Currently we don't to know whether table is reference or not so
    // we are using hardcoded list of reference tables. It means that custom
    // reference tables aren't supported.
    if ($this->isReferenceTable($this->table)) {
      return;
    }

    if (in_array(Schema::SHARD_FIELD, $this->insertFields)) {
      // Exit if shard id field already exists.
      return;
    }

    $this->insertFields[] = Schema::SHARD_FIELD;
    if (!empty($this->insertValues)) {
      foreach ($this->insertValues as $key => $values) {
        $this->insertValues[$key][] = $this->getDefaultShardIdValue();
      }
    }
    else {
      // If there are no values, then this is a default-only query.
      // We still need to handle that.
      $this->insertValues[][] = $this->getDefaultShardIdValue();
    }
  }

  /**
   * Determine is current table reference or not.
   */
  protected function isReferenceTable($table) {
    $reference_tables = Schema::referenceTables();
    if (in_array($table, $reference_tables)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function from(SelectInterface $query) {
    if (!$this->isReferenceTable($this->table)) {
      $this->addShardIdSelect($query);
    }
    $this->fromQuery = $query;

    return $this;
  }

  /**
   * Adds shard field to the select query if it doesn't exist.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The select query.
   */
  protected function addShardIdSelect(SelectInterface $query) {
    $fields = $query->getFields();
    $shard_field_exists = FALSE;
    // Need to check whether shard key field exists.
    foreach ($fields as $field) {
      if (isset($field[Schema::SHARD_FIELD])) {
        $shard_field_exists = TRUE;
        break;
      }
    }

    if (!$shard_field_exists) {
      foreach ($fields as $field) {
        if (!$this->isReferenceTable($field['table'])) {
          // Add Shard field to the first non-reference table.
          $query->addField($field['table'], Schema::SHARD_FIELD);
          break;
        }
      }
    }
  }

  /**
   * Gets the default value for shard_id field if it isn't provided.
   */
  protected function getDefaultShardIdValue() {
    // @todo: what value for shard_id will be used by default?
    return \Drupal::time()->getRequestTime();
  }

}
