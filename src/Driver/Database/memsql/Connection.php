<?php

namespace Drupal\memsql\Driver\Database\memsql;

use Drupal\Core\Database\Driver\mysql\Connection as MysqlConnection;

/**
 * MemSQL implementation of \Drupal\Core\Database\Connection.
 */
class Connection extends MysqlConnection {

  /**
   * Provides a map of condition operators to condition operator options.
   *
   * @var array
   */
  protected static $memsqlConditionOperatorMap = [
    'LIKE' => ['postfix' => ''],
    'NOT LIKE' => ['postfix' => ''],
  ];

  /**
   * {@inheritdoc}
   */
  public function mapConditionOperator($operator) {
    return isset(static::$memsqlConditionOperatorMap[$operator]) ? static::$memsqlConditionOperatorMap[$operator] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function driver() {
    return 'memsql';
  }

  /**
   * {@inheritdoc}
   */
  public function databaseType() {
    return 'memsql';
  }

  /**
   * {@inheritdoc}
   */
  public function queryTemporary($query, array $args = [], array $options = []) {
    $tablename = $this->generateTemporaryTableName();
    $this->query('CREATE TEMPORARY TABLE {' . $tablename . '} ' . $query, $args, $options);
    return $tablename;
  }

}
