<?php

namespace Drupal\memsql\Driver\Database\memsql;

use Drupal\Core\Database\Query\Merge as QueryMerge;

/**
 * MemSQL implementation of \Drupal\Core\Database\Query\Merge.
 */
class Merge extends QueryMerge {}
