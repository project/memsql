<?php

namespace Drupal\memsql\Driver\Database\memsql;

use Drupal\Core\Database\Transaction as DatabaseTransaction;

/**
 * MemSQL implementation of \Drupal\Core\Database\Transaction.
 */
class Transaction extends DatabaseTransaction {}
