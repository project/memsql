<?php

namespace Drupal\memsql\Driver\Database\memsql;

use Drupal\Core\Database\Query\Update as QueryUpdate;

/**
 * MemSQL implementation of \Drupal\Core\Database\Query\Update.
 */
class Update extends QueryUpdate {}
