# MemSQL driver for Drupal 8

## Module installation

Install module as usual.

After installation copy memsql/drivers folder to the Drupal root folder (on the same level with index.php file) because Drupal searches database driver there.

If you use composer, you can add command to the composer.json file to update driver each time when memsql module is updated (in this example `web` is a docroot folder):
```
"post-update-cmd": [
    "cp -r web/modules/contrib/memsql/drivers web/"
]
```

```php
function hook_schema() {
  $schema['example_distributed_table'] = [
    'description' => 'Distributed table example.',
    'fields' => [
      'nid' => [
        'description' => 'The {node}.nid this record affects.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'shard_id' => [
        'description' => 'Shard id',
        'type' => 'varchar_ascii',
        'length' => 12,
        'not null' => TRUE,
        'default' => '',
      ],
    ],
    'primary key' => ['nid'],
    'shard key' => ['shard_id'],
  ];

  $schema['example_reference_table'] = [
    'description' => 'Reference table example',
    'type' => 'reference',
    'fields' => [
      'nid' => [
        'description' => 'The {node}.nid this record affects.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['nid'],
  ];
  return $schema;
}
```

CREATE TABLE `test` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `shard_id` bigint(11),
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`, `shard_id`),
  UNIQUE KEY `name` (`name`, `shard_id`),
  SHARD KEY (`shard_id`)
);


CREATE REFERENCE TABLE `test_reference` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
);
